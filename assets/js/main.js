window.addEventListener('resize', resizeFunction)
window.addEventListener('load', resizeFunction) 

function resizeFunction() {
    setTimeout(function() {
        console.log('timeout')
        if ($(window).width() > 992) {
            // $('.foot-pow').prepend($('.foot-pow').children().last());
            $('.foot-pow-right:parent').each(function () {
                $(this).insertBefore($(this).prev('.foot-pow-left'));
                $('.foot-pow-right').toggleClass('switch');
            });
        }
        if ($(window).width()>768 && $(window).width()<=992) {
            // $('.foot-pow').prepend($('.foot-pow').children().last());
            $('.foot-pow-left').attr('style', 'background:none');
            $('.foot-pow-right:parent').each(function () {
                $(this).insertBefore($(this).prev('.foot-pow-left'));
                $('.foot-pow-right').toggleClass('switch');
            });
            $('.foot-pow').attr('style', 'background:#1157b5');
            $('.foot-pow-right h6').attr('style', 'text-align: left');
        }
        
        
        if($(window).width()<768){
            $(".city-banner p").text(function (index, currentText) {
                return currentText.substr(0, 80) + '...';
            });
            $(".cit-ban-it p").text(function (index, currentText) {
                return currentText.substr(0, 50) + '...';
            });
            $(".city-n-top p").text(function (index, currentText) {
                return currentText.substr(0, 80) + '...';
            });
            $(".city-n-bot p").text(function (index, currentText) {
                return currentText.substr(0, 30) + '...';
            });
        }
        
        if($(window).width()>768){
            $(".cit-ban-it p").text(function (index, currentText) {
                return currentText.substr(0, 80) + '...';
            });
        }
        
        if($(window).width()>768 && $(window).width()<=1024){
            $(".city-n-bot p").text(function (index, currentText) {
                return currentText.substr(0, 30) + '...';
            });
        }
        
        // RESIZING FONT CITY NAVIGATION FOR 992PX-1021PX
        if($(window).width()>992 && $(window).width()<=1021){
            $('.nav-city-bx a').attr('style', 'font-size: 15px; !important');
        }
        
        // LIMITING FONT HEADLINE IN BEST-NEWS PAGE FOR 768PX-992PX
        if($(window).width()>768 && $(window).width()<=992){
            $(".best-n-caption h4").text(function (index, currentText) {
                return currentText.substr(0, 30) + '...';
            });
        }
    }, 100)
}

$(document).ready(function(){

/*========================================================
        1. INDEX PAGE - 4 TOP NEWS SECTION
    Swapping places of news when are clicked to larger div
=========================================================*/
function swapContent1() {
    if ($(window).width() > 1200) {
        var tempContent = $("div.news-1").html();
        $("div.news-1").empty().html($("div.news-2").html());
        $("div.news-2").empty().html(tempContent);
    }
}

function swapContent2() {
    if ($(window).width() > 1200) {
        var tempContent = $("div.news-1").html();
        $("div.news-1").empty().html($("div.news-3").html());
        $("div.news-3").empty().html(tempContent);
    }
}

function swapContent3() {
    if ($(window).width() > 1200) {
        var tempContent = $("div.news-1").html();
        $("div.news-1").empty().html($("div.news-4").html());
        $("div.news-4").empty().html(tempContent);
    }
}


/*========================================================
        1. INDEX PAGE - SWAPPING CONTENT
    Swapping footer credits content for 992px and higher 
=========================================================*/

$(document).ready(function () {
    
    
});

/*========================================================
        1. INDEX PAGE - NAVIGATION
=========================================================*/

$(document).ready(function () {

    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });
});

$(document).ready(function () {
    $(document).scroll(function () {
        var $nav = $(".navbar");
        var navCit = $(".nav-city");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        navCit.toggleClass('scrolled', $(this).scrollTop() > navCit.height());
    });
});

/*========================================================
        1. CITYS NAVIGATION - ICON ROTATION
=========================================================*/

$(".cit-btn-rotate").click(function () {
    $(".cit-rotate").toggleClass("down");
});

/*========================================================
        1. BEST NEWS PAGE - LIMITER
    Letters limited to 80 letters per news card
=========================================================*/

$(".best-n-caption p").text(function (index, currentText) {
    return currentText.substr(0, 80) + '. . .';
});

// $(".cit-ban-li p").text(function (index, currentText) {
//     return currentText.substr(0, 80) + '...';
// });





});


$('.nav-city-bx').each(function() {
    $(this).on('click', function() {
        $(this).addClass('active_link')
        $(this).siblings().removeClass('active_link')
    })
})

$('.nav-link').each(function() {
    $(this).on('click', function() {
        $(this).addClass('active_navlink')
        $(this).siblings().removeClass('active_navlink')
    })
})